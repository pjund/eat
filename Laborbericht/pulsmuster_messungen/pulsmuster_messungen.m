
clc
clear
close all
% Grundfreq Takt - 5.7. elim - 5.7.11.13. elim - sin grob - sin fein

I_eff = [6.296 5.752 5.581 6.281 5.911]
I_h1  = [5.394  5.333   5.345   5.962   5.814]
thd_i = [59.33 40.72  30.04  32.73   17.19]
P_in  = [1307.3 1262    1240    1273.3  1254.9]
n     = [730.1  730.6   730.1   716.5   719.8]
M     = [14.2   13.98   13.88   13.96   14.05]
P_out = M.*2.*pi.*n./60
P_ver = P_in-P_out
eta   = P_out./P_in

subplot(2,2,1)
hold on
plot(I_eff,'x-')
plot(I_h1,'o-')
hold off
xticks([1 2 3 4 5])
xticklabels({'Grund Freq. Taktung','5. & 7. Eliminiert','5., 7., 11.& 13 Elminiert','Sin grob','Sin fein'})
ax = gca;
ax.XTickLabelRotation = -40;
legend('I_{eff}[A]','I_{mu=1}[A]')
ylabel('[A]')
title('Strom')


subplot(2,2,2)
plot(thd_i,'o-')
ylabel('[%]')
xticks([1 2 3 4 5])
xticklabels({'Grund Freq. Taktung','5. & 7. Eliminiert','5., 7., 11.& 13 Elminiert','Sin grob','Sin fein'})
ax = gca;
ax.XTickLabelRotation = -40;
legend('THD [%]')
title('Klirrfaktor')

subplot(2,2,3)
hold on
plot(P_in,'x-')
plot(P_out,'o-')
plot(P_ver,'*-')
hold off
xticks([1 2 3 4 5])
xticklabels({'Grund Freq. Taktung','5. & 7. Eliminiert','5., 7., 11.& 13 Elminiert','Sin grob','Sin fein'})
ax = gca;
ax.XTickLabelRotation = -40;
legend('P_{ein}[W]','P_{aus}[W]','P_{verlust}')
ylabel('[W]')
title('Leistung')


subplot(2,2,4)
plot(eta,'o-')
xticks([1 2 3 4 5])
xticklabels({'Grund Freq. Taktung','5. & 7. Eliminiert','5., 7., 11.& 13 Elminiert','Sin grob','Sin fein'})
ax = gca;
ax.XTickLabelRotation = -40;
legend('Eta')
ylabel('[%]')
title('Wirkungsgrad')
%%
clc
clear
I_eff = [6.33 5.95 5.681];
I_h1  = [5.49 5.43 5.437];
thd_i = [0.5795 0.446 0.2966];
P_in  = [899.7*1.4639 893*1.4639 858.0*1.4639]
n     = [732.4 732 729.8];
M     = [14.56 14.58 14.30];
P_out = M.*2.*pi.*n./60
P_ver = P_in-P_out
eta   = P_out./P_in

1256/858