%EAT Selbstgefuehrte Stromrichter

%Uebung 6.5 Optimierte Pulsmuster

%Eliminierung 3. Harmonische
clc
clear
close all
M = 1.0
U_dc = 100;
v_kill = 5
solution = 1;
% I:     U1 = M*U_dc/2 = 4/pi * U_dc/2 * (1 - 2*cos(v*alpha1) + 2*cos(v*alpha2))

% II:    U_kill = 0 = 4/pi * U_dc/2 /V_kill * (1 - 2*cos(v_kill*alpha1) + 2*cos(v_kill*alpha2)) 

% Aus I => alpha2 = acos( (M*Pi/4 - 1)/2 + cos(alpha1))

% I in II einsetzen:
%        
%         1 - 2*cos(v_kill*alpha1) + 2*cos(v_kill*acos( (M*Pi/4 - 1)/2 + cos(alpha1))) = 0
res = 0.1/2/pi;
alpha1 = 0:res:2*pi;
x = 1 - 2*cos(v_kill*alpha1) + 2*cos(v_kill*acos( (M*pi/4 - 1)/2 + cos(alpha1)));

% get zeros
[x_zero_value, x_zero_loc] = findpeaks(-abs(x))

% plot zeros
plot(alpha1,x,'x',x_zero_loc*res,x_zero_value,'o')

% get solutions
alpha1_solution = alpha1(x_zero_loc)
alpha2_solution = acos( (M*pi/4 - 1)/2 + cos(alpha1_solution))

% generate signal
p = 1;
swp = zeros(1,10);
swp(1) = alpha1_solution(solution);
swp(2) = alpha2_solution(solution);
swp(3) = pi - alpha2_solution(solution);
swp(4) = pi - alpha1_solution(solution);
swp(5) = pi;
swp(6) = pi + alpha1_solution(solution);
swp(7) = pi + alpha2_solution(solution);
swp(8) = 2*pi - alpha2_solution(solution);
swp(9) = 2*pi - alpha1_solution(solution);
swp(10) = 2*pi;

N = length(0:res:2*pi)
pulse = zeros(1,N);
m = 1;
for n=1:N
    if(2*pi/N*n >= swp(m))
        p = -p;
        m = m+1;
        
    end
    pulse(n) = p;
end

plot(0:res:2*pi,pulse)

xlim([0.00 2*pi])
ylim([-1.10 1.10])

% plot fourier transform
w = 2*pi * (0:(N-1)) / N;
w2 = fftshift(w);
w3 = unwrap(w2 - 2*pi);

figure(1)
stem(w3,abs(fftshift(fft(pulse)))/N*2)
legend('Simulation')

xlim([0 0.532])

alpha1_solution(solution)*360/(2*pi)
alpha2_solution(solution)*360/(2*pi)

% Messung Modulation 1, 5Harmonische killed, solution 1

measured1 = [1 0.003 0.0022 0.01 0.01 0.0218 0.4254 0.0038 0.0198 0.0051 0.34 0.0028 0.0792 0.0008 0.0072];
stem(measured1)
legend('Gemessen')
title('M=1, 5. Harmonische eliminiert, 1. Loesung')
xlabel('N-Harmonische')
ylabel('Amplitude')
xlim([0 15])

fft = abs(fftshift(fft(pulse)))/N*2;
simulated = fft(199:213);

x = 1:15;
y = [measured1;simulated]';

stem(x,y)
title('FFT Pulsmuster - M=1, 5. Harmonische eliminiert, 1. Loesung')
legend('Gemessen','Simuliert')
xlabel('N-Harmonische')
ylabel('Amplitude')
xlim([0 15])


%% Messung Modulation 1, 5Harmonische killed, solution 2

measured2 = [1 0.0047 0.022 0 0.005 0 0.4037 0 0 0 0.2987 0 0.0609 0 0]
stem(measured2)
legend('Gemessen')
title('M=1, 5. Harmonische eliminiert, 2. Loesung')
xlabel('N-Harmonische')
ylabel('Amplitude')
xlim([0 15])

%fft = abs(fftshift(fft(pulse)))/N*2;
%simulated = fft(199:213);

x = 1:15;
y = [measured2;simulated]';

stem(x,y)
legend('Gemessen','Simuliert')
title('FFT Pulsmuster - M=1, 5. Harmonische eliminiert, 2. Loesung')
xlabel('N-Harmonische')
ylabel('Amplitude')
xlim([0 15])
