%% plot csv script from KO
clear, clc, close all;
prompt = 'Liegt die Messung im Frequenz oder Zeitbereich?\n (1) Zeitbereich\n (2) Frequenzbereich\n';
par = input(prompt);
%parse user input
if(par == 1)
pathname = uigetdir();
cd(pathname);
files = dir('*.CSV');
msg = sprintf('\n\nEs wurden %d Kanaele gefunden\n',length(files));
fprintf(msg);
figure(1)
for n=1:length(files)
    res = csvread(files(n).name,0,3);
    data(n).time=res(1:end,1);
    data(n).time = data(n).time - min(data(n).time);
    data(n).signal=res(1:end,2);
    
    subplot(length(files),1,n)
    plot(data(n).time,data(n).signal);
    string = sprintf('Channel %d',n);
    title(string);
    xlabel('Time [ms]')
    ylabel('Voltage [V]') 
end
currentDirectory = pwd;
 [~, folderName, ~] = fileparts(currentDirectory);
  
saveas(gcf, ['plots_', folderName], 'epsc2')
print('-dpdf','-r0',['plots_', folderName ]);

elseif(par == 2)
pathname = uigetdir();
cd(pathname);
files = dir('*.CSV');
msg = sprintf('\n\nEs wurde %d Kanal gefunden\n',length(files));
fprintf(msg);
hFig = figure(1)
for n=1:length(files)
    res = csvread(files(n).name,0,3);
    data(n).frequency=res(1:end,1);
    data(n).magnitude=res(1:end,2);
    
    subplot(length(files),1,n)
    plot(data(n).frequency,data(n).magnitude);
    %title('FFT of Phase Voltage');
    xlabel('Frequency [Hz]')
    ylabel('Magnitude [dB]') 
    axis([0 max(data(n).frequency) -inf inf])
    hold on
    for f=1:2:21
    val = f*25; %value to find
    tmp = abs(val-data(n).frequency);
    [~, idx] = min(tmp); %index of closest value
%     if idx+1<length(data(n).frequency)
%     idx = idx+1;
%     end
    
    plot(data(n).frequency(idx),data(n).magnitude(idx),'or');
    set(hFig, 'Position', [200 200 800 140])
    end
    hold off
end

%%
currentDirectory = pwd;
 [~, folderName, ~] = fileparts(currentDirectory);
  
saveas(gcf, ['plots_', folderName], 'epsc2')
print('-dpdf','-r0',['plots_', folderName ]);

end
 
