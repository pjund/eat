%% Uebung 2 zur Asynchronmaschine, stationaeres Verhalten
% Parameteridentifikation ASM
clc
clear
Uvk = 400; %V
f = 50; %Hz
Iph = 15; %A
cosphi = 0.741;
p = 6.9e3; %W
u = 1450; %1/min
R1 = 0.7; %Ohm

% Leerlaufmessung (s=0 => Rs=inf)
P = 300; %W
S = 6e3; %VA

% Stillstandsmessung (s=1 => Rs=0) bei 5Hz und 15A
P = 880; %W
S = 903; %VA

% 1. Berechnen Sie die Polpaarzahl der Maschine, die mechanische Winkelgeschwindigkeit 
% im Nennarbeitspunkt und den Schlupf s bei Nennbetrieb.

% Polpaarzahl
p = round(2*60*f/u);
fprintf(1,'Polpaarzahl: %d \n', p)

% mechanische Winkelgeschwindigkeit
% omega = 2*pi*fm
omega = 2*pi*u/60;

% Schlupf
us = 1500
s = u/us;


%2. Bestimmen Sie aus der Stillstandsmessung den auf die Statorseite 
%transformierten Rotorwiderstand R2' und die Streuinduktivit?ten 
%L1 und L2'. Die beiden Streu- induktivit?ten werden als gleich gross 
%angenommen. RFe und Lh kann vernachl?ssigt werden.
% Stillstandsmessung (s=1 => Rs=0) bei 5Hz und 15A
P = 880; %W
S = 903; %VA


R2t = (P - Iph^2*R1)/Iph^2

Q = sqrt(S^2-P^2)

Xl = Q/Iph

Xl = Q/Iph^2
Lg = Xl / (2*pi*5)
L1 = Lg/2
L2 = L1


% 3. Bestimmen Sie aus der Leerlaufmessung die Hauptinduktivit?t 
% Lh und den Verlustwiderstand RFe.
% Leerlaufmessung (s=0 => Rs=inf)
P = 300; %W
S = 6e3; %VA

PRFe =  P - R1*(P/Uvk)^2 
RFe = PRFe / (P/Uvk)^2






