clc
clear
%% LEA
% ?bungen 19.9.16

% A. Leistungen

% Nr. 3

% Eine dreiphasige Last wird am sinusf?rmigen Netz mit einer 
% Phasenspannung von 230 V (Effektivwert) betrieben. 
% Der aufgenommene Strom ist sinusf?rmig und sein Effektivwert betr?gt 10 A. 
% Er eilt der Spannung um 30? nach.

Up = 230; %V
I = 10; %A
phi = 30; %?

% 3. Berechnen Sie die aufgenommene Schein-,Wirk-und Blindleistung

S = Up*I
P = Up*I*cos(2*pi/360*phi)
Q = Up*I*sin(2*pi/360*phi)
Q2 = sqrt(S^2-P^2)

% 4. Hat die Last induktives oder kapazitives Verhalten?
% A: Induktiv, da der strom nacheilt wie bei einer spule

% Eine dreiphasige Last wird am Netz mit einer verketteten Spannung 
% von 11 kV (Effektivwert) betrieben. Der aufgenommene sinusf?rmige 
% Strom betr?gt 200 A (Effektivwert) und ist in Phase zur Netzspannung.

% 5. Berechnen Sie die aufgenommene Schein- ,Wirk- und Blindleistung.

Uvk = 11e3;
I = 200;
Up = Uvk / sqrt(3)
S = 3 * Uvk / sqrt(3) * I
S2 = sqrt(3) * Uvk * I
P = S
Q = 0

% B. Mechanik

