clc

close all
w = 2*pi * (0:(length(simout.Data)-1)) / length(simout.Data);
w2 = fftshift(w);
w3 = unwrap(w2 - 2*pi);

figure(1)
plot(tout,simout.Data,tout,simout1.Data)

figure(2)
stem(w3,abs(fftshift(fft(simout.Data)))/length(simout.Data)*2)
legend('Simulation')
