%%ASM am Netz
clear, clc, close all;

R_ph = 0.759/0.98
R_vk = 1.49/0.98
omega = 50*2*pi;
% Messung mit S=0

U_vk_s0 = 404.6;
P_s0 = 107;       %W
S_s0 = 2.83e3;    %VA

% Messung mit s = 1

U_vk_s1 = 91.89;
I_s1 = 12.83;

S_s1 = U_vk_s1 * I_s1 * sqrt(3);
P_s1 = 980;


%% Bestimmung Maschinenparameter mit s = 0 

I_ph = S_s0/U_vk_s0/sqrt(3);
cos_phi = P_s0/S_s0;
phi = acos(cos_phi);
U_l_ges = U_vk_s0/sqrt(3)*sin(phi);
L_ges = U_l_ges/I_ph/omega;

%% Bestimmung Maschinenparameter mit s = 1

U_ph_s1 = U_vk_s1 / sqrt(3);
U_R1 = I_s1 * R_ph;
U_R2 = P_s1/3/I_s1-U_R1;
R2_t = U_R2/I_s1;

cos_phi = P_s1/S_s1;
phi = acos(cos_phi);
U_l_2_sigma = U_vk_s1/sqrt(3)*sin(phi);

L_sigma = U_l_2_sigma/I_s1/omega/2;

%% Messungen Motor

M = [0      5       10         15         20        25       30     35      40];
n = [1499   1495    1490       1489       1480      1475     1469   1463    1459];
I = [4.03   4.26    4.7893     5.56       6.45      7.50     8.63   9.85    11.11];    
P = [240    1008    1770       2560       3360      4150     4970   5800    6620]; 
S = [2830   2990    3350       3900       4530      5250     6000   6850    7700];
s = (omega/(2*pi)*60/2-n)/1500;
cos_phi = P./S; 
Q = S.*sin(acos(cos_phi));
P_mech = (n./(60)*2*pi).*M;
P_v = P-P_mech;
eta = P_mech./P;
figure(1)
subplot(2,2,1)
plot(M,n)
legend('n [1/min]')

subplot(2,2,2)
plot(M,I)
legend('I [A]')

subplot(2,2,3)
plot(M,P,M,S,M,Q,M,P_mech,M,P_v)
legend('P [W]','S [VA]','Q [VAr]','P_{mech} [W]','P_{v}')

subplot(2,2,4)
plot(M,eta,M,cos_phi)
legend('eta','cos(\phi)')



%% Messung Generator
clc
M = [0      5       10      15      20      25      30      35      40];
n = [1500   1503    1508    1513    1518    1522    1528    1531    1539];
I = [4.03   4.14    4.54    5.2     6.04    6.91    7.88    8.92    9.95];
P = [233    -530    -1287   -2045   -2800   -3540   -4260   -4970   -5680];
S = [2837   2900    3200    3660    4237    4860    5530    6270    6970];

cos_phi = -P./S; 
Q = S.*sin(acos(cos_phi));
P_mech = (n./(60)*2*pi).*M;
P_v = P_mech+P;
eta = -P./P_mech;   
figure(2)
subplot(2,2,1)
plot(M,n)
legend('n [1/min]')

subplot(2,2,2)
plot(M,I)
legend('I [A]')

subplot(2,2,3)
plot(M,P,M,S,M,Q,M,P_mech,M,P_v)
legend('P [W]','S [VA]','Q [VAr]','P_{mech} [W]','P_{v}')

subplot(2,2,4)
plot(M,eta,M,cos_phi)
legend('eta','cos(\phi)')


%% Messung Stromortskurve
clc
