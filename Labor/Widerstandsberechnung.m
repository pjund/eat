%Labor 3Ph WR Berechung Widerstand DC 
clear, close all, clc;
P_NennDC = 7400; %W
U_NennDC = 440; %V
I_NennDC = 21; %A
Omega_NennDC = 2320*2*pi/60; %rad/s

P_NennASM = 5500; %W
Omega_NennASM = 1450*2*pi/60; %rad/s
P = 2

Omega1 = 2*pi*50
M = 15;

s_Nenn = (Omega1/P-Omega_NennASM)/(Omega1/P)
M_Nenn = P_NennASM/Omega_NennASM
%angenommen linearer Betrieb:
ds__dm = s_Nenn/M_Nenn;
omega2 = 2*pi*25
omega = (1-ds__dm*M)*omega2/P;
rpm = omega/(2*pi)*60

%lineare Zunahme von U_DC zu omega
du__domegaDC = U_NennDC/(Omega_NennDC);
U0_DC = du__domegaDC * omega
I_DC = (omega*M)/P_NennDC*I_NennDC
R_g = U0_DC/I_DC;
R_s = 2.8;
R_L = R_g - R_s





