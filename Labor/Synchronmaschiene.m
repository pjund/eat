%% EAT Synchronmaschiene
clc
clear
% 6.3 SM als Generator im Inselbetrieb
% 6.3.1 Belastungskennlinie

n = 1500;   %1/min
I_erreger = 1;  %A


% Mit ohmscher Last
phi_polrad = [-44 -42.4 -40.5 -37.6 -34.2 -28.9 -25.6]; % Random polradwinkel
phi_polrad = 45+phi_polrad

U_last = [187 184.9 180.52 174.6 164.8 152.2 142.6];
I_last = [7 7.79 8.73 9.85 11.26 12.92 13.83];

R_last = U_last./I_last;

plot(R_last,phi_polrad,R_last,3*I_last.^2.*R_last)
xlabel('R_{last}')
ylabel('phi_{polrad}')

% Mit induktiver Last
phi_polrad = []; % Random polradwinkel
phi_polrad = 45+phi_polrad

U_last = [];
I_last = [];

R_last = U_last./I_last;

plot(R_last,phi_polrad,R_last,3*I_last.^2.*R_last)
xlabel('R_{last}')
ylabel('phi_{polrad}')


