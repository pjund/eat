close all, clc, clear;
%Last bei 25Hz INVERTRON
M = [0.43   1       1.53    1.97     2.52    3.01    3.49    4.01    4.52    5.03    6.05    7.04    8.04    8.99    10      12      14      16      18      20];    
n = [744    743     743     742     742     741     741     740     740     739     738     737     736     735     734     732     731     729     727     725];
I = [4.304   4.327  4.348   4.365   4.398   4.426   4.444   4.48    4.53    4.555   4.63    4.742   4.874   4.932   5.06    5.284   5.62    5.79    6.188   6.485];
U = [331.1   332.0  332.2   331.8   333.4   332.5   332.4   332.2   332.2   331.8   332.8   333.8   335.2   336.0   336.1   335.2   336.2   336     336.3   335.9];
P = [275.5   310    355     397.2   438     478     502.8   547.8   589.1   630     710     790     870     940     1026    1194    1342    1504    1666    1825];
figure(1)
subplot(3,1,1);
plot(M,n,'b');
xlabel('Torque [Nm]');
ylabel('rpm [1/min]');
subplot(3,1,2);
plot(M,I,'r');
xlabel('Torque [Nm]');
ylabel('Current [A]');
subplot(3,1,3);
plot(M,P,'g');
xlabel('Torque [Nm]');
ylabel('Power [W]');

%U/f Kennlinie bei 5 Nm INVERTRON
f =    [0       5     10     15     20      25      30      35      40      45      50];
U_ph = [55.56   100    134.74 160.19 179.2   193.1   205.4   220     235.6   249     262];
U_wr = [278     281      278.09 278.36 278.5   278.58  278.6   278.58  279.4   280.3   280.98]; 
figure(2)
plot(f,U_ph,f,U_wr)
legend('Phasenspannung [V]','Wechselrichterausgansspannung [V]');
xlabel('Frequenz [Hz]');

