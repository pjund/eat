% Kommutierung Diodenbr?ckengleichrichter
clear, clc, close all;
f = 50; %[Hz]
T = 1/f;
stepsize = 0.00001;
t = 0:stepsize:2*(1/f);
U_netz = sqrt(2)*400*sin(t*2*pi*f); %[V]
i_last = 1; %[A] 
L_netz = 10; %[H]
figure(1);
plot(t,U_netz,'b');
ylabel('Spannung');
xlabel('Zeit');
title('Netzspannung');


% theoretischer Kommutierungsstrom im Netz
i_d14 = 1/L_netz * 1/T*cumsum(U_netz)*stepsize;
i_d23 = 1/L_netz * 1/T*cumsum(-U_netz)*stepsize;
figure(2)
plot(t,i_d14,'r');
hold on
plot(t,i_d23,'b');

%limitiert durch den maximalen Storm durch den Verbraucher


i_d14(i_d14>i_last) = i_last;
i_d23(i_d23>i_last) = i_last;
i_netz = i_d14 - i_d23;
figure(3)
plot(t,i_d14,'c');
hold on
plot(t,i_d23,'m');

plot(t,i_netz,'y');

 